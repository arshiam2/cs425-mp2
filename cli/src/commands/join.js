// @flow
const path = require("path");
const chalk = require("chalk");

const handleErrors = require("../utils/handleErrors");

module.exports.command = "join [number]";
module.exports.describe = "Adds the specified VM to the group";

module.exports.builder = (yargs: any) => yargs;

const PORT = 20000;
const HOST = "172.22.156.15";

const dgram = require("dgram");
const process = require("process");

const socket = dgram.createSocket({ type: "udp4", reuseAddr: true });

socket.bind(PORT);

function sendMessage(number) {
  // console.log(number)
  const message = Buffer.from(`join ${number + "" + Date.now()}`);
  socket.send(message, 0, message.length, PORT, HOST, function() {});
}

module.exports.handler = handleErrors(async (argv: {}) => {
  console.log("Sending message to introducer")
  let number = argv.number
  await sendMessage(number);
  process.exit(0);
});
