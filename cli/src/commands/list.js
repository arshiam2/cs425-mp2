// @flow
const path = require("path");
const chalk = require("chalk");

const handleErrors = require("../utils/handleErrors");

module.exports.command = "list [number]";
module.exports.describe = "Lists the membership list local the specified VM.";

module.exports.builder = (yargs: any) => yargs;

const PORT = 5000;
const HOST = "172.22.158.17";


const dgram = require("dgram");
const process = require("process");

const socket = dgram.createSocket({ type: "udp4", reuseAddr: true });

const machineToIps = {
  "0": "172.16.138.158",
  "1": "172.22.156.15",
  "2": "172.22.158.15",
  "3": "172.22.154.16",
  "4": "172.22.156.16",
  "5": "172.22.158.16",
  "6": "172.22.154.17",
  "8": "172.22.158.17",
  "9": "172.22.154.18",
  "10": "172.22.156.18"
};

socket.bind(PORT);

socket.on("listening", function() {
  socket.setBroadcast(true);
  const address = socket.address();
  console.log(
    `UDP socket listening on ${address.address}:${address.port} pid: ${
      process.pid
    }`
  );
});


function sendMessage(host, number) {
  const message = Buffer.from(`list`);
  console.log(host);
  socket.send(message, 0, message.length, 20000, host, function() {});
}

module.exports.handler = handleErrors(async (argv: {}) => {
  console.log("Sending message to introducer");
  let number = argv.number;
  await sendMessage(machineToIps[number], number);
});
