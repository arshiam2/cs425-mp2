// @flow
const path = require("path");
const chalk = require("chalk");

const handleErrors = require("../utils/handleErrors");

module.exports.command = "leave [number]";
module.exports.describe = "Voluntarily removal of the specified VM from group";

module.exports.builder = (yargs: any) => yargs;

const PORT = 20000;
const MULTICAST_ADDR = "233.255.255.255";
const HOST = "172.22.156.15";

const dgram = require("dgram");
const process = require("process");

const socket = dgram.createSocket({ type: "udp4", reuseAddr: true });

const machineToIps = {
  "0": "172.16.138.158",
  "1": "172.22.156.15",
  "2": "172.22.158.15",
  "3": "172.22.154.16",
  "4": "172.22.156.16",
  "5": "172.22.158.16",
  "6": "172.22.154.17",
  "8": "172.22.158.17",
  "9": "172.22.154.18",
  "10": "172.22.156.18"
};

socket.bind(PORT);

function sendMessage(number) {
  const message = Buffer.from(`leave ${number}`);
  socket.send(message, 0, message.length, PORT, HOST, function() {});
}


function sendMessageToNode(number) {
    const message = Buffer.from(`leave ${number}`);
    console.log(machineToIps[number])
    console.log(message.toString())
    socket.send(message, 0, message.length, PORT, machineToIps[number], function() {});
}

module.exports.handler = handleErrors(async (argv: {}) => {
  console.log("Sending message to introducer");
  let number = argv.number;
  await sendMessage(number);
  await sendMessageToNode(number);
});
