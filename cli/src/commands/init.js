// @flow
const path = require("path");
const chalk = require("chalk");

const handleErrors = require("../utils/handleErrors");

module.exports.command = "init";
module.exports.describe = "Sets up five nodes (VMs 2 and 3) in a group, with VM 1 as the introducer.";

module.exports.builder = (yargs: any) => yargs;

const PORT = 20000;
const HOST = "172.22.156.15";

const dgram = require("dgram");
const process = require("process");

const socket = dgram.createSocket({ type: "udp4", reuseAddr: true });

socket.bind(PORT);

function sendMessage() {
  const message = Buffer.from(`init`);
  socket.send(message, 0, message.length, PORT, HOST, function() {});
}

module.exports.handler = handleErrors(async (argv: {}) => {
  console.log("Sending message to introducer")
  await sendMessage();
  process.exit(0);
});
