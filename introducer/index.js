const PORT = 20000;
const MULTICAST_ADDR = "233.255.255.255";

const dgram = require("dgram");
const process = require("process");
var ip = require("ip");

const socket = dgram.createSocket({ type: "udp4", reuseAddr: true });

const CLI_HOST = "172.16.138.158";

const CLI_PORT = "5000";

socket.bind(PORT);

let connections = {};

let machines = ["2" + Date.now(), "3" + Date.now()];

const machineToIps = {
  "0": "172.16.138.158",
  "1": "172.22.156.15",
  "2": "172.22.158.15",
  "3": "172.22.154.16",
  "4": "172.22.156.16",
  "5": "172.22.158.16",
  "6": "172.22.154.17",
  "8": "172.22.158.17",
  "9": "172.22.154.18",
  "10": "172.22.156.18"
};

const IpsToMachine = {
  "0.0.0.0": "0",
  "172.16.138.158": "0",
  "172.22.156.15": "1",
  "172.22.158.15": "2",
  "172.22.154.16": "3",
  "172.22.156.16": "4",
  "172.22.158.16": "5",
  "172.22.154.17": "6",
  "172.22.158.17": "8",
  "172.22.154.18": "9",
  "172.22.156.18": "10"
};

let machineNumber = IpsToMachine[ip.address()];

socket.on("listening", function() {
  socket.addMembership(MULTICAST_ADDR);
  socket.setBroadcast(true);
  const address = socket.address();
  console.log(
    `UDP socket listening on ${address.address}:${address.port} pid: ${
      process.pid
    } machine ${machineNumber}`
  );
});


function initialize(machineNumber, connectedTo) {
  const message = Buffer.from(`initialize [${connectedTo}]`);
  socket.send(
    message,
    0,
    message.length,
    PORT,
    machineToIps[machineNumber],
    function() {}
  );
}

function getPairings(machines) {
  for (let i = 0; i < machines.length; i++) {
    surroundingNodes = [];

    if (i + 1 > machines.length - 1) {
      surroundingNodes.push(machines[i + 1 - machines.length]);
    } else {
      surroundingNodes.push(machines[i + 1]);
    }

    if (i + 2 > machines.length - 1) {
      surroundingNodes.push(machines[i + 2 - machines.length]);
    } else {
      surroundingNodes.push(machines[i + 2]);
    }

    if (i - 1 < 0) {
      surroundingNodes.push(machines[i - 1 + machines.length]);
    } else {
      surroundingNodes.push(machines[i - 1]);
    }

    if (i - 2 < 0) {
      surroundingNodes.push(machines[i - 2 + machines.length]);
    } else {
      surroundingNodes.push(machines[i - 2]);
    }

    console.log("")
    console.log(
      "Node :" +
        machines[i] +
        " is connect to " +
        Array.from(new Set(surroundingNodes))
    );
    console.log("")
    initialize(machines[i][0], Array.from(new Set(surroundingNodes)));
  }
  
}

socket.on("message", function(message, rinfo) {
  const text = message.toString().split(" ");
  if (text[0] === "init") {
    getPairings(machines);
  }
  if (text[0] === "join") {
    if (
      machines
        .map(m => m.toString()[0])
        .filter(f => f === text[1].toString()[0]).length == 0
    ) {
      machines.push(text[1]);
      console.log("Current Machines Up " + machines)
      getPairings(machines);
    } else {
      ("This machine has already joined");
    }
  }

  if (text[0] === "leave") {
    machines = machines.filter(m => m.toString()[0] !== text[1]);
    getPairings(machines);
  }

  if (text[0] === "dead") {
    console.log("")
    console.log("Killed " + text[1].toString()[0])
    console.log("")
    const ondLength = machines.length 
    machines = machines.filter(m => m.toString()[0] !== text[1].toString()[0]);
    console.log("Current Machines Up " + machines)
    console.log("")
    getPairings(machines);

  }
});
