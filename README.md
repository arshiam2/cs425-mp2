# CS 425 Membership


## Prerequisites
Have node and npm installed on each VM and clone the repo in each one. 

## Cli Tool

The cli tool has a couple commands that allows it to interact with the introducers and machines to tell it to join, leave, or list that machine's membership lists.

To set it up, run the following commands 
```
cd cli
npm install
npm run build
npm link
```

This links the ```mem``` command to your command line, so you can run all the commands below

```mem -h``` Explains all the commands availible

```mem init``` Sets up five nodes (VMs 2 through 6) in a group, with VM 1 as the introducer. 

```mem join <VM_number>```: Adds the specified VM to the group. 

```mem leave <VM_number>```: Voluntarily removal of the specified VM from group. 

```mem list <VM_number>```: Lists the membership list local the specified VM. 


## Machine

Each machine has a membership lists of nodes its pinging and statuses of whether or not they are alive or dead depending on how long it has been since the last ping. 

To set it up, run the following commands 
```
cd machine
npm install
npm run start
```




## Introducer

The introducer has a list of all VMs up, and gets messages when anotehr machine joins and leaves. Based on that, it updates all the nodes membership lists. 

To set it up, run the following commands 
```
cd introducer
npm install
npm run start
```


# Getting logs 

In the introducer and machine, instead of running ```npm run start``` as the last command, run ```npm run start > log.txt``` which will write all the output to  a file. Otherwise, if it is open, you can the logs printed out to temrinal. 